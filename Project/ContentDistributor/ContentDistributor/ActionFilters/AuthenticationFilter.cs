﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using ContentDistributor.Authenticator;

namespace ContentDistributor.ActionFilters {
    public class AuthenticationFilter : ActionFilterAttribute {
        public override void OnActionExecuting(ActionExecutingContext filterContext) {
            if (SecurePageFilter.SecurePage((Contact)filterContext.HttpContext.Session["Contact"], filterContext.RequestContext.HttpContext.Request.Cookies))
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "Controller", "Voucher" }, { "Action", "Index" } });
        }
    }
}