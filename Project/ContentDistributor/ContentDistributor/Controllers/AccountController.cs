﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ContentDistributor.Authenticator;
using ContentDistributor.ActionFilters;
using ContentDistributor.Models;

namespace ContentDistributor.Controllers {
    public class AccountController : Controller {
        public const string MSG_USER_NOT_AUTHENTICATED = "There was an error while authenticating your e-mail / password. Please try again.";

        //
        // GET: /Login/
        [AuthenticationFilter]
        public ActionResult Index() {
            return View();
        }

        //
        // POST: /Login/

        [AuthenticationFilter]
        [HttpPost]
        public ActionResult Index(string email, string password) {
            bool succeed = false;
            int attempts = 0;

            while (!succeed && attempts < 10) {
                try {
                    using (AuthenticatorClient authenticator = new AuthenticatorClient()) {
                        string tokenAccess = authenticator.GenerateAccessToken(email, password);
                        Authenticator.Contact c = authenticator.VerifyUser(email, password);

                        if (c.Id > 0) {
                            Session["Contact"] = c;
                            Response.SetCookie(new HttpCookie("ACCESS_TOKEN", tokenAccess));
                            return RedirectToAction("Index", "Voucher");
                        } else ViewBag.Error = MSG_USER_NOT_AUTHENTICATED;
                    }
                    succeed = true;
                } catch (Exception ex) {
                    ViewBag.Error = MSG_USER_NOT_AUTHENTICATED;
                }
                attempts++;
            }
            
            return View();
        }

        public ActionResult Register() {
            return View();
        }

        [HttpPost]
        public ActionResult Register(String GivenName, String FamilyName, int Day, int Month, int Year, String Email, String Password) {
            CommonContact contact = CommonContact.CreateCommonContact(0, 1, GivenName, FamilyName, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, Email, String.Empty, false, false, Password, String.Empty, false, String.Empty, DateTime.Now, false, System.Guid.NewGuid());

            using (PlanetshakersEntities context = new PlanetshakersEntities()) {
                try {
                    context.AddToCommonContacts(contact);
                    context.SaveChanges();
                    Session["MessageSuccess"] = "<strong>Congratulations!</strong> You have successfully registered for a Planetshakers account!";
                    return RedirectToAction("Index");
                } catch (Exception ex) { }
            }

            return View();
        }

        public ActionResult LogOut() {
            Session["Contact"] = null;
            Response.Cookies.Clear();
            return RedirectToAction("Index");
        }
    }
}