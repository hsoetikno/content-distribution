﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;
using System.Data.Objects.DataClasses;

using ContentDistributor.ActionFilters;
using ContentDistributor.Authenticator;
using ContentDistributor.Models;

namespace ContentDistributor.Controllers {
    public class VoucherController : Controller {
        public const string SYNTAX_VOUCHER_CODE = "<<V-CODE>>";

        public const string MSG_VOUCHER_NOT_EXIST = "The voucher " + SYNTAX_VOUCHER_CODE + " does not exist. Please make sure that the voucher code entered is right and try again.";
        public const string MSG_VOUCHER_EXPIRES = "The voucher " + SYNTAX_VOUCHER_CODE + " is not available for redemption. The voucher is either invalid or has passed its expiry date.";
        public const string MSG_VOUCHER_EMPTY = "An error occured while redeeming the voucher " + SYNTAX_VOUCHER_CODE + ", please try again. If the problem persists, please refer to our <a href=\"#\">help page</a>.";
        public const string MSG_VOUCHER_REDEEMED = "The voucher " + SYNTAX_VOUCHER_CODE + " was already redeemed in your account. Refer to <a href=\"#\">my content</a> to view all your redeemed content.";

        //
        // GET: /Voucher/

        [SecurePageFilter]
        public ActionResult Index() {

            Authenticator.Contact contact = (Authenticator.Contact)Session["Contact"];
            int countProduct = 0;
            DataModel dm = new DataModel();

            countProduct = (from p in dm.Products
                            where p.Name == "Pick It Up"
                            select p).Count();

            // If no apple object, populate database with test data.
            if (countProduct < 1) {

                // *** *** *** *** *** *** *** *** *** ***
                //           POPULATE PRODUCTS
                // *** *** *** *** *** *** *** *** *** ***

                Product product = new Product();
                product.Name = "Pick It Up";
                product.Description = "Recorded live at the 10th anniversary conference. Featuring best-selling songs like Healer and Beautiful Saviour, PICK IT UP is a must have album.";
                product.PreviewDescription = "Recorded live at the 10th anniversary conference.";
                product.UrlThumbnail = "http://i50.tinypic.com/an1t7m.png";
                dm.AddToProducts(product);

                product = new Product();
                product.Name = "Free";
                product.Description = "Recorded live at Planetshakers City Church. This album features planet-shaking praise and worship songs like Free, I Need You and Greatly to be Praised.";
                product.PreviewDescription = "Recorded live at Planetshakers City Church.";
                product.UrlThumbnail = "http://i49.tinypic.com/1hcb6c.jpg";
                dm.AddToProducts(product);

                dm.SaveChanges();

                // *** *** *** *** *** *** *** *** *** ***
                //           POPULATE VOUCHERS
                // *** *** *** *** *** *** *** *** *** ***

                Voucher v = new Voucher();
                v.DateActive = DateTime.Now;
                v.DateExpiry = DateTime.Now.AddMonths(1);
                v.VoucherCode = "PICKITUP06";

                Product prod = (from p in dm.Products
                                where p.Name == "Pick It Up"
                                select p).First();

                Item f = new Item();
                f.Name = "Worship You Alone";
                f.ItemNo = 1;
                f.ProductId = prod.ID;
                f.FileURL = "https://dl.dropbox.com/s/61yzbnlakr0ogc3/11%20Worship%20You%20Alone.m4a?dl=1";

                v.Items = new EntityCollection<Item>();
                v.Items.Add(f);
                v.ProductId = prod.ID;

                dm.AddToVouchers(v);
                dm.SaveChanges();

                prod = (from p in dm.Products
                        where p.Name == "Free"
                        select p).First();

                v = new Voucher();
                v.DateActive = DateTime.Now;
                v.DateExpiry = DateTime.Now.AddMonths(1);
                v.VoucherCode = "FREE08";
                v.Items = new EntityCollection<Item>();
                v.ProductId = prod.ID;

                f = new Item();
                f.Name = "Intro";
                f.ItemNo = 1;
                f.FileURL = "https://dl.dropbox.com/s/sgsbi766jz1uirg/Intro.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Salvation";
                f.ItemNo = 2;
                f.FileURL = "https://dl.dropbox.com/s/jumbojzqn8rrq9w/Salvation.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Saved the Day";
                f.ItemNo = 3;
                f.FileURL = "https://dl.dropbox.com/s/vbolkrtch9c4gd9/Saved%20The%20Day.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Free";
                f.ItemNo = 4;
                f.FileURL = "https://dl.dropbox.com/s/31iz2t0c4dabwh7/Free.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Greatly to be Praised";
                f.ItemNo = 5;
                f.FileURL = "https://dl.dropbox.com/s/vjp8ejlybd60kzf/Greatly%20To%20Be%20Praised.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "In the Highest";
                f.ItemNo = 6;
                f.FileURL = "https://dl.dropbox.com/s/6cdegywtaieouzw/In%20The%20Highest.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Calvary";
                f.ItemNo = 7;
                f.FileURL = "https://dl.dropbox.com/s/mlfgryek1a4d3c6/Calvary.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "I Need You";
                f.ItemNo = 8;
                f.FileURL = "https://dl.dropbox.com/s/c8qcscag0zshzog/I%20Need%20You.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Shout Your Name";
                f.ItemNo = 9;
                f.FileURL = "https://dl.dropbox.com/s/cn3k8cvwlfus6cl/Shout%20Your%20Name.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "For Everything";
                f.ItemNo = 10;
                f.FileURL = "https://dl.dropbox.com/s/olkhsc6pb6n8jbh/For%20Everything.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "My Hope";
                f.ItemNo = 11;
                f.FileURL = "https://dl.dropbox.com/s/s7hhwtyc4trxy76/My%20Hope.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Saving Grace";
                f.ItemNo = 12;
                f.FileURL = "https://dl.dropbox.com/s/nm7tamqulb0olun/Saving%20Grace.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "Glory to God";
                f.ItemNo = 13;
                f.FileURL = "https://dl.dropbox.com/s/om7kds0nh0ftr2c/Glory%20To%20God.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                f = new Item();
                f.Name = "A Worship Moment";
                f.ItemNo = 14;
                f.FileURL = "https://dl.dropbox.com/s/3xk2i5p36157ae4/A%20Worship%20Moment.mp3?dl=1";
                f.ProductId = prod.ID;
                v.Items.Add(f);

                v.Product = (from p in dm.Products
                             where p.Name == "Free"
                             select p).First();

                dm.AddToVouchers(v);

                VoucherRedemption r = new VoucherRedemption();
                r.ContactId = contact.Id;
                r.RedemptionDate = DateTime.Now;
                r.VoucherCode = v.VoucherCode;

                dm.AddToVoucherRedemptions(r);

                dm.SaveChanges();
            }

            List<String> vouchers = (from v in dm.VoucherRedemptions
                                     where v.ContactId == contact.Id
                                     orderby v.RedemptionDate descending
                                     select v.VoucherCode).ToList();

            ViewBag.Contact = contact;
            ViewBag.Vouchers = new List<Voucher>();

            foreach (string code in vouchers) {
                Voucher voucher = (from v in dm.Vouchers
                                   where v.VoucherCode == code
                                   select v).First();
                ((List<Voucher>)ViewBag.Vouchers).Add(voucher);
            }

            return View();
        }

        [SecurePageFilter]
        public ActionResult View(String VoucherCode) {
            DataModel dm = new DataModel();
            Voucher voucher = (from v in dm.Vouchers
                                where v.VoucherCode == VoucherCode
                                select v).First();

            ViewBag.ProductName = voucher.Product.Name;
            ViewBag.ProductDescription = voucher.Product.Description;
            ViewBag.Thumbnail = voucher.Product.UrlThumbnail;
            ViewBag.Items = voucher.Items;

            return View();
        }

        public ActionResult Redeem() {
            return View();
        }

        [HttpPost]
        [SecurePageFilter]
        public ActionResult Redeem(String voucherCode) {
            Voucher voucher = new Voucher();
            
            voucherCode = voucherCode.ToUpper();

            Authenticator.Contact contact = (Authenticator.Contact)Session["Contact"];

            using (DataModel context = new DataModel()) {
                var vouchers = (from v in context.Vouchers
                                where v.VoucherCode == voucherCode
                                select v);

                var redeemedVouchers = (from v in context.VoucherRedemptions
                                        where v.ContactId == contact.Id && v.VoucherCode == voucherCode
                                        select v);

                if (vouchers.Count() == 1) voucher = vouchers.First();

                // VALID-01: Voucher is still active
                if (voucher == null || !VoucherUtility.IsVoucherActive(voucher)) {
                    ViewBag.RedemptionSuccess = false;
                    ViewBag.Error = MSG_VOUCHER_EXPIRES.Replace(SYNTAX_VOUCHER_CODE, voucherCode);
                    return View();
                }

                // VALID-02: Voucher has never been redeemed
                if (redeemedVouchers.Count() > 0 ) {
                    ViewBag.RedemptionSuccess = false;
                    ViewBag.Error = MSG_VOUCHER_REDEEMED.Replace(SYNTAX_VOUCHER_CODE, voucherCode);
                    return View();
                }

                // VALID-03: Voucher contains items
                if (voucher.Items.Count() < 1) {
                    ViewBag.RedemptionSuccess = false;
                    ViewBag.Error = MSG_VOUCHER_EMPTY.Replace(SYNTAX_VOUCHER_CODE, voucherCode);
                    return View();
                }

                if (voucher.VoucherCode == voucherCode) {
                    VoucherRedemption redemption = new VoucherRedemption();
                    redemption.VoucherCode = voucher.VoucherCode;
                    redemption.ContactId = contact.Id;
                    redemption.RedemptionDate = DateTime.Now;
                    context.AddToVoucherRedemptions(redemption);

                    try {
                        context.SaveChanges();
                        return RedirectToAction("RedeemSuccess");
                    } catch (Exception ex) {
                        ViewBag.RedemptionSuccess = false;
                    }
                } else ViewBag.RedemptionSuccess = false;
            }

            return View();
        }

        [SecurePageFilter]
        public ActionResult RedeemSuccess(String voucherCode) {
            Authenticator.Contact contact = (Authenticator.Contact)Session["Contact"];

            using (DataModel context = new DataModel()) {
                int countVouchers = (from r in context.VoucherRedemptions
                                     where r.ContactId == contact.Id && r.VoucherCode == voucherCode
                                     select r).Count();

                if (countVouchers > 0) ViewBag.VoucherCode = voucherCode;
            }

            return View();
        }
    }
}
