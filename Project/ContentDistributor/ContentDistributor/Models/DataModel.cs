﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations;

namespace ContentDistributor.Models {

    public class Product {
        public int ID { get; set; }
        public string Name { get; set; }
        public string UrlThumbnail { get; set; }
        public string Description { get; set; }
        [MaxLength(200)]
        public string PreviewDescription { get; set; }
    }

    public class Voucher {
        public int ID { get; set; }
        public string VoucherCode { get; set; }
        public int ProductId { get; set; }
        public DateTime DateExpiry { get; set; }
        public DateTime DateActive { get; set; }

        public virtual ICollection<Item> Items { get; set; }
        public virtual Product Product { get; set; }
    }

    public class Item {
        public int ID { get; set; }
        public int ProductId { get; set; }
        public int ItemNo { get; set; }
        public string Name { get; set; }
        public string FileURL { get; set; }

        public virtual ICollection<Voucher> Vouchers { get; set; }
        public virtual Product Product { get; set; }
    }

    [Table("VoucherRedemptionHistory")]
    public class VoucherRedemption {
        public int ID { get; set; }
        public int ContactId { get; set; }
        public string VoucherCode { get; set; }
        public DateTime RedemptionDate { get; set; }
    }

    public class DataModel : DbContext {
        public DbSet<Product> Products { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }
        public DbSet<Item> Files { get; set; }
        public DbSet<VoucherRedemption> VoucherRedemptionHistory { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Voucher>()
                .HasMany(v => v.Items)
                .WithMany(i => i.Vouchers)
                .Map(m => 
                {
                    m.MapLeftKey("VoucherId");
                    m.MapRightKey("ItemId");
                    m.ToTable("VoucherItems");
                });

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }

    public class DataModelInitializer : DropCreateDatabaseIfModelChanges<DataModel> {
        protected override void Seed(DataModel context) {
            base.Seed(context);
        }
    }
}