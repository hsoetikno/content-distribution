﻿using System;

using ContentDistributor.Models;

namespace ContentDistributor {
    class VoucherUtility {
        public static bool IsVoucherActive(Voucher v) {
            return v.DateActive <= DateTime.Now && (v.DateExpiry == null || v.DateExpiry > DateTime.Now);
        }
    }
}